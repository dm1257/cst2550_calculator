import org.junit.*;
import static org.junit.Assert.*;

public class CalculatorTest{

    @Test
    public void testAdd(){
	double n1 = 2.5;
	double n2 = 0;
	double expected = 2.5;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testSubtract(){
	double expectedResult = 1;
	assertEquals(expectedResult, Calculator.subtract(2, 1), 1e-6);
    }
        @Test
    public void testMultiply(){
	double n1 = 2;
	double n2 = 2;
	double expected = 4;
	double result = Calculator.multiply(n1, n2);
	assertEquals(expected, result, 1e-6);
    }
    @Test
    public void testDivide(){
	double n1 = 14;
	double n2 = 7;
	double expected = 2;
	double result = Calculator.divide(n1, n2);
	assertEquals(expected, result, 1e-6);
    }
    @Test
    public void testAbsolute(){
	double n = -5;
	double expected = 5;
	double result = Calculator.abs(n);
	assertEquals(expected, result, 1e-6 );
    }
    @Test
    public void testAbsolute2(){
	double n = 25;
	double expected = 25;
	double result = Calculator.abs(n);
	assertEquals(expected, result, 1e-6 );
	
    }

    @Test
    public void testPower(){
	double n = 2;
	int  m = 2;
	double expected = 4;
	double result = Calculator.power(n, m);
	assertEquals(expected, result, 1e-6);
    }
    
    @Test
    public void testNegPower(){
	double n = 2;
	int  m = -1;
	double expected = 0.5;
	double result = Calculator.power(n, m);
	assertEquals(expected, result, 1e-6);
    }

      
}
